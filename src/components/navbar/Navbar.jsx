import React, { useContext } from 'react';
import './Navbar.scss';
import foodIcon from '../../assets/icons/food.svg';
import cartIcon from '../../assets/icons/shopping_cart.svg';
import { Link }  from 'react-router-dom';
import { ShopContext } from '../../contexts/shopContext/ShopContext';
import { LoginContext } from '../../contexts/loginContext/LoginContext';

const Navbar = props => {
    const shopContext = useContext(ShopContext);
    const loginContext = useContext(LoginContext)

    const cartProductNumber = shopContext.cart.reduce((count, curItem) => {
        return count + curItem.quantity;
      }, 0);
    
    return ( 
        <nav className="navbar">
            <h1>Digifood</h1>
            { loginContext.isLogged === true && 
                <ul>
                    <li>
                        <Link to="/">
                        <img src={foodIcon} alt="produits"/>
                        </Link>
                    </li>
                    <li>
                        <Link to="/panier">
                            <img src={cartIcon} alt="panier"/>
                        </Link>
                        <span>{cartProductNumber}</span>
                    </li>
                    <li>
                        <button className="Button__signOut"
                            onClick={() => loginContext.signOut()}>
                            <p>Déconnexion</p>
                        </button>
                    </li>
                </ul>
            }

            { loginContext.isLogged === false &&
                <ul>
                    <li>
                        <button className="Button__signIn"
                            onClick={() => loginContext.signIn()}>
                            <p>Connexion</p>
                        </button>
                    </li>
                </ul>
            }
        </nav>
     );
}
 
export default Navbar;