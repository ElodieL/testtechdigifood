import React from 'react';
import Product from '../product/Product';
import './ProductsList.scss';
import { useContext } from 'react';
import { TabContext } from '../../contexts/tabContext/TabContext';
import { ShopContext } from '../../contexts/shopContext/ShopContext';

const ProductsList = props => {
    const tabContext = useContext(TabContext);
    const shopContext = useContext(ShopContext);
    return ( 
        <>
            {props.list.map((item, index) =>
                <div key={index}>        
                    {tabContext.activTab === item.name && 
                        <>
                            <h2>{item.name}</h2>
                            <div className="productsList" >
                                {item.products.map(product =>
                                    <div key={product.id}>
                                        <Product className="productsList__product" 
                                            name={product.name}
                                            price={product.price}
                                            image={product.image}
                                            onClick={() => shopContext.addProductToCart(product)}
                                        />
                                    </div>    
                                )}
                            </div>
                        </>
                    }  
                </div>
            )}
        </>
     );
}
 
export default ProductsList;