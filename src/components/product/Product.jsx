import React from 'react';
import addIcon from '../../assets/icons/add_shopping_cart.svg';
import './Product.scss';

const Product = props => {
    

    return ( 
        <div className="product">
            <h4 className="product__name">{props.name} <span>{props.quantity}</span></h4>
            <img className="product__picture" src={props.image} alt={props.name}/>
            <button className="product__button" onClick={props.onClick}>
                <img src={addIcon} alt={props.price}/>
                <p>{props.price} €</p>
            </button>
        </div>
     );
}
 
export default Product;