import React, { useContext } from 'react';
import './Tabsbar.scss';
import { TabContext } from '../../contexts/tabContext/TabContext';

const Tabsbar = props => {
    const tabContext = useContext(TabContext);

    const tabStyle = (name) => {
        let style;
        if(tabContext.activTab === name) {
            style = "activ";
        } 
        return style;
    }

    return ( 
        <nav className="tabsbar">
            {props.tabs.map((tab, index) =>
                <button className={tabStyle(tab.name)}
                    key={index} 
                    onClick={() => tabContext.openTab(tab.name, tab.isActiv)}>
                    {tab.name}
                </button>
            )}
        </nav>
    );
}
 
export default Tabsbar;