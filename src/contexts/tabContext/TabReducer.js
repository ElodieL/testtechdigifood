export const OPEN_TAB = 'OPEN_TAB';

const openTab = (tabName, state) => {
    const updatedActivTab = tabName;
    return {...state, activTab: updatedActivTab};
};

export const TabReducer = (state, action) => {
    switch(action.type) {
        case OPEN_TAB:
            return openTab(action.tabName, state);
        default:
            return state;
    };
};