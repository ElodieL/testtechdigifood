import React, { createContext, useReducer } from 'react';
import productsList from '../../data/productsList.json';
import { TabReducer, OPEN_TAB } from './TabReducer.js';


export const TabContext = createContext({});

export const TabState = props => {
    const defaultActivatedTab = productsList.categories[0].name;
    const [tabState, dispatch] = useReducer(TabReducer, { activTab : defaultActivatedTab});

    const openTab = (tabName) => {
        setTimeout(() => {
            dispatch({type: OPEN_TAB, tabName : tabName});
        }, 500);
    }

    return (
        <TabContext.Provider 
            value={{
                activTab: tabState.activTab,
                openTab: openTab,
            }}>
           {props.children}
        </TabContext.Provider>

    )
}