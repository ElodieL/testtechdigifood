export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';

const signIn = state => {
    let updatedIsLogged = true;
    console.log(updatedIsLogged);
    return {...state, isLogged: updatedIsLogged};
};

const signOut = state => {
    let updatedIsLogged = false;
    console.log(updatedIsLogged);
    return {...state, isLogged: updatedIsLogged};
};


export const LoginReducer = (state, action) => {
    switch(action.type) {
        case SIGN_IN:
            return signIn(state);
        case SIGN_OUT:
            return signOut(state);
        default:
            return state;
    };
};