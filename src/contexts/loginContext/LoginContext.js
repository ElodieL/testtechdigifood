import React, { createContext, useReducer } from 'react';
import { LoginReducer, SIGN_IN, SIGN_OUT } from './LoginReducer';

export const LoginContext = createContext({});

export const LoginState = props => {
    const [loginState, dispatch] = useReducer(LoginReducer, { isLogged: false });

    const signIn = () => {
        setTimeout(() => {
            dispatch({ type: SIGN_IN });
            console.log('connecté')
        }, 800);
    };

    const signOut = () => {
        setTimeout(() => {
            dispatch({ type: SIGN_OUT });
            console.log('déconnecté')
        }, 800);
    };

    return (
        <LoginContext.Provider 
            value={{
                isLogged: loginState.isLogged,
                signIn: signIn,
                signOut: signOut,
            }}>
            {props.children}
        </LoginContext.Provider>
    )
}