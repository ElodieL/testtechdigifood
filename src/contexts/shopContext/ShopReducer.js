export const ADD_PRODUCT = 'ADD_PRODUCT';
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
export const RESET_CART = 'RESET_CART';

const addProductToCart = (product, state) => {
    const updatedCart = [...state.cart];
    let updatedTotalCart = state.totalCart;
    const updatedItemIndex = updatedCart.findIndex(
      item => item.id === product.id
    );

    if (updatedItemIndex < 0) {
        updatedCart.push({ ...product, quantity: 1, totalPrice: product.price });
        updatedTotalCart = (updatedTotalCart + product.price);
    } else {
        const updatedItem = {
            ...updatedCart[updatedItemIndex]
    };
        updatedItem.quantity++;
        updatedItem.totalPrice = (updatedItem.totalPrice + product.price);
        updatedCart[updatedItemIndex] = updatedItem;
        updatedTotalCart = (updatedTotalCart + product.price);
    };
    return {...state, cart: updatedCart, totalCart: updatedTotalCart};
};

const removeProductFromCart = (productId, state) => {
    const updatedCart = [...state.cart];
    let updatedTotalCart = state.totalCart;
    const updatedItemIndex = updatedCart.findIndex(
        item => item.id === productId
    );

    const updatedItem = {
        ...updatedCart[updatedItemIndex]
    };
        updatedItem.quantity--;
        updatedItem.totalPrice = (updatedItem.totalPrice - updatedItem.price);
        updatedTotalCart = (updatedTotalCart - updatedItem.price);
    if (updatedItem.quantity <= 0) {
        updatedCart.splice(updatedItemIndex, 1);
    } else {
        updatedCart[updatedItemIndex] = updatedItem;
    };
    return {...state, cart: updatedCart, totalCart: updatedTotalCart};
};

const resetCart = state => {
    alert('Etes-vous sûr de vouloir vider votre panier?');
    const updatedCart = [];
    const updatedTotalCart = 0;
    return {...state, cart: updatedCart, totalCart: updatedTotalCart};
};


export const ShopReducer = (state, action) => {
    switch(action.type) {
        case ADD_PRODUCT:
            return addProductToCart(action.product, state);
        case REMOVE_PRODUCT:
            return removeProductFromCart(action.productId, state);
        case RESET_CART:
            return resetCart(state);
        default:
            return state;
    }
};