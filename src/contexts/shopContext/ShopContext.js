import React, { createContext, useReducer } from 'react';
import { ShopReducer, ADD_PRODUCT, REMOVE_PRODUCT, RESET_CART } from './ShopReducer';
import productsList from '../../data/productsList.json';

export const ShopContext = createContext({});

export const ShopState = props => {
    const products = productsList.categories;
    const [ cartState, dispatch ] = useReducer(ShopReducer, { cart: [], totalCart: 0 });

    const addProductToCart = product => { 
        setTimeout(() => {
            dispatch({ type: ADD_PRODUCT, product: product });
        }, 300);
    };

    const removeProductFromCart = productId => {
        setTimeout(() => {
            dispatch({ type: REMOVE_PRODUCT, productId: productId });
        }, 300);
    };

    const resetCart = () => {
        setTimeout(() => {
            dispatch({ type: RESET_CART });
        }, 300);
    };

    return (
        <ShopContext.Provider
            value={{
                products: products,
                cart: cartState.cart,
                totalCart: cartState.totalCart,
                addProductToCart: addProductToCart,
                removeProductFromCart: removeProductFromCart,
                resetCart: resetCart,
            }}
            >
            {props.children}
        </ShopContext.Provider>
    );
    
}