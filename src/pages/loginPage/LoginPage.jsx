import React from 'react';
import './LoginPage.scss';
import { useContext } from 'react';
import { LoginContext } from '../../contexts/loginContext/LoginContext';
import { Redirect } from 'react-router-dom';

const LoginPage = () => {
    const loginContext = useContext(LoginContext);

    if (loginContext.isLogged === false ) {
        return ( 
            <div className="LoginPage">
                <h1 className="LoginPage__title">Digifood v2</h1>
                <form className="LoginPage__form"  >
                    <label>
                       <p>Adresse email : </p> 
                       <input type="email" placeholder="email"/>
                    </label>
                    <label>
                        <p>Mot de passe : </p>
                        <input type="password" placeholder="mot de passe"/>
                    </label>
               </form>     
                <button className="LoginPage__submit" onClick={() => loginContext.signIn()}>
                    <p>Connexion</p>
                </button>
            </div>
         );
    } else {
        return (
            <Redirect to="/"/>
        )
    }
}
 
export default LoginPage;