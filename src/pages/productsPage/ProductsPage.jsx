import React, { useContext } from 'react';
import './ProductsPage.scss';
import { ShopContext } from '../../contexts/shopContext/ShopContext';
import Tabsbar from '../../components/Tabsbar/Tabsbar';
import ProductsList from '../../components/productsList/ProductsList';

const ProductsPage = () => {
    const shopContext = useContext(ShopContext);
    const products = shopContext.products;

    return ( 
        <div className="productsPage">
            <div>
                <Tabsbar tabs={products}/>
                <ProductsList list={products}/>
            </div>   
        </div> 
    );
}
 
export default ProductsPage;