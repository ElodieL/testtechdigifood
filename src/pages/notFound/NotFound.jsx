import React from 'react';
import './NotFound.scss';
import { Link } from 'react-router-dom';

const NotFound = () => {
    return ( 
        <div className="NotFound">
            <h1>Erreur 404 : Pas de panique ! Cette page n'existe simplement pas.</h1>
            <Link className="LifeBuoy" to='/'> Cliquez ici pour revenir sur une page du site </Link>
        </div>
     );
}
 
export default NotFound;