import React from 'react';
import { useContext } from 'react';
import { ShopContext } from '../../contexts/shopContext/ShopContext';
import removeIcon from '../../assets/icons/remove_shopping_cart.svg';
import cartIcon from '../../assets/icons/shopping_cart.svg';
import paymentIcon from '../../assets/icons/payment.svg';
import plusIcon from '../../assets/icons/add.svg';
import minusIcon from '../../assets/icons/remove.svg';
import './CartPage.scss';

const CartPage = () => {
    const shopContext = useContext(ShopContext);
    return ( 
      <div className="cartPage">
        <div>
        {shopContext.cart.length <= 0 ? (
          <h2>
            Votre panier est vide.
          </h2> 
            ) : (
              <div>
                <header className="cartPage__header">
                  <h2 className="cartPage__title"> 
                    <img src={cartIcon} alt="panier"/>
                    Mon panier
                  </h2>

                  <button className="cartPage__deleteButton" onClick={() => shopContext.resetCart()}>
                    <img src={removeIcon} alt="vider"/>
                    <p>vider</p>
                  </button>
                  
                  <button className="cartPage__payButton">
                    <img src={paymentIcon} alt="payer"/>
                    <p>{shopContext.totalCart} €</p>
                  </button>

                </header>
                <ul className="cartList">
                  
                  {shopContext.cart.map(cartItem => (
                    <li key={cartItem.id}>
   
                              

                                <div className="cartItem">
                                    <img className="cartItem__picture" src={cartItem.image} alt={cartItem.name}/>
                                    <h4 className="cartItem__name">{cartItem.name}</h4>
                                    <h4 className="cartItem__price">{cartItem.totalPrice} €</h4>

                                      
                                      <button className="cartItem__button" onClick={() => shopContext.removeProductFromCart(cartItem.id) }>
                                      <img src={minusIcon} alt="plus"/>
                                      </button>
                                      <p>{cartItem.quantity}</p>
                                      <button className="cartItem__button" onClick={() => shopContext.addProductToCart(cartItem)}>
                                      <img src={plusIcon} alt="moins"/>
                                      </button>
                                </div>

                              
                    </li>
                  ))}
                </ul>
              </div>
            )}
          </div>
        </div>      
      );
  }
 
export default CartPage;