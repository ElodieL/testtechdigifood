import React, { useContext } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import ProductsPage from '../pages/productsPage/ProductsPage';
import CartPage from '../pages/cartPage/CartPage';
import NotFound from '../pages/notFound/NotFound';
import LoginPage from '../pages/loginPage/LoginPage';
import Navbar from '../components/navbar/Navbar';
import { LoginContext } from '../contexts/loginContext/LoginContext';

const Routing = () => {
    const loginContext = useContext(LoginContext);

    const PrivateRoute = ({component: Component, ...rest}) => (
        <Route {...rest} render={ props => (
            loginContext.isLogged === true ? <Component {...props}/> : <Redirect to="/login" />
        )}/>
    );
    
    return ( 
        <>
            <Router>
                <div className="App">
                    <header className="App__header">
                        <Navbar/>
                    </header>
                    <main className="App__main">
                        <Switch>
                            <Route path="/login" exact component={LoginPage}/>
                            <PrivateRoute path="/" exact component={ProductsPage}/>
                            <PrivateRoute path="/panier" exact component={CartPage}/>
                            <Route path="*" component={NotFound}/>
                        </Switch>
                        </main>
                    <footer className="App__footer">

                    </footer>
                </div>
            </Router>
        </>
     );
}
 
export default Routing;