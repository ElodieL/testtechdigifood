import React from 'react';
import './App.scss';
import { ShopState } from '../contexts/shopContext/ShopContext';
import { TabState } from '../contexts/tabContext/TabContext';
import { LoginState } from '../contexts/loginContext/LoginContext';
import Routing from './Routing';

function App() {
  return (
    <ShopState>
    <TabState>
    <LoginState>
        <Routing/>
    </LoginState>
    </TabState>
    </ShopState>
  );
}

export default App;
